#include <iostream>
using namespace std;

class Empty {};

class EmptyDerived : public Empty {};

class NonEmpty : public Empty {
    private:
        char c;
};

struct Composite {
public:
    Empty a;
    int b;
};

struct CompositeChar {
public:
    Empty a;
    char b;
};

int main()
{
    Empty empty = Empty();
    EmptyDerived emptyD = EmptyDerived();
    NonEmpty nonEmpty = NonEmpty();
    Composite composite;
    CompositeChar compositeChar;

    cout << "Size of Empty object: " << sizeof(empty)<< endl <<
    "Size of EmptyDerived object: " << sizeof(emptyD) << endl  <<
    "Size of NonEmpty object: " << sizeof (nonEmpty) << endl << 
    "Size of Composite object: " << sizeof(composite) << endl <<
    "Size of composite.a: " << sizeof(composite.a) << endl <<
    "Size of composite.b: " << sizeof(composite.b) << endl <<
    "Size of CompositeChar object: " << sizeof(compositeChar) << endl <<
    "Size of compositeChar.a: " << sizeof(compositeChar.a) << endl <<
    "Size of compositeChar.b: " << sizeof(compositeChar.b) << endl;    
    cout << "Address of composite.a: " << &composite.a << endl <<
    "Address of composite.b: " << &composite.b << endl <<
    "Address of composite: " << &composite << endl;
    return 0;
}
