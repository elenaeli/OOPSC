#ifndef NUMVECTOR2_H
#define NUMVECTOR2_H
#include <vector>

class NumVector2 
{
public:
	NumVector2(int );
	virtual ~NumVector2();
	double norm() const;
	double& operator[](const int);
	const double &operator[](const int i) const;
	int size() const;
	
private:
	std::vector<double> v;
};
   
#endif