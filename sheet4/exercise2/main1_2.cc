#include <vector>
#include <iostream>
#include <cmath>
#include "NumVector2.h"
using namespace std;

int main() {
	NumVector2 v(3);
	v[0]=1; v[1]=3, v[2]=4;

	cout << "norm is "<< v.norm() << endl;
	cout << "vector is [" << v[0] << ", " << v[1] << ", " << v[2] << "]" << endl;
	const NumVector2 w = v;
	cout << "norm is "<< w.norm() << endl;
	cout << "vector is [" << w[0] << ", " << w[1] << ", " << w[2] << "]" << endl;

}