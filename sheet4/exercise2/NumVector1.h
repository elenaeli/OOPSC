#ifndef NUMVECTOR1_H
#define NUMVECTOR1_H
#include <vector>

class NumVector1 : private std::vector<double> 
{
public:
	NumVector1(int );
	virtual ~NumVector1();
	double norm() const;
	using std::vector<double>::operator[];

};
#endif