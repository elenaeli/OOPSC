#include <vector>
#include <iostream>
#include <cmath> 
#include "NumVector1.h"

NumVector1::NumVector1(int size) : std::vector<double>(size) {
}
NumVector1::~NumVector1() {	
}

double NumVector1::norm() const
{
    double result = 0.0;
    for(int i = 0; i < this->size(); i++) {
        result += (this->at(i) * this->at(i));
    }
    return sqrt(result);
}