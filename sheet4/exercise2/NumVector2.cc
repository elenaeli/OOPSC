#include <vector>
#include <iostream>
#include <cmath> 
#include <stdexcept>
#include "NumVector2.h"

NumVector2::NumVector2(int size) {
	v = std::vector<double>(size); 
}

NumVector2::~NumVector2() {	
}

double NumVector2::norm() const
{
    double result = 0.0;
    for(int i = 0; i < v.size(); i++) {
        result += (v[i] * v[i]);
    }
    return sqrt(result);
}

double& NumVector2::operator[] (const int i)
{
	if(i>=0 && i<v.size())
    	return v[i];
    else
    	throw std::out_of_range("Array index is out of bounds" );
}

const double& NumVector2::operator[](const int i) const
{
	if(i>=0 && i<v.size())
    	return v[i];
    else
    	throw std::out_of_range("Array index is out of bounds" );
}

int NumVector2::size() const
{
	return v.size();
}
