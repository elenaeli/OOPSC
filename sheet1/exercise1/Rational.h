/** A header file to represent a rational number with its methods and variables
*/

#ifndef RATIONAL_H
#define RATIONAL_H
#include <ostream>

using namespace std;
class Rational {

private:
	int numerator;
	unsigned int denominator;

public:
	//3 different constructors and a destructor
	Rational ();
	Rational(int, int);
	Rational(int);
	~Rational();
	//overload uniary operators
	Rational operator+=(Rational R);
	Rational operator-=(Rational R);
	Rational operator*=(Rational R);
	Rational operator/=(Rational R);
	Rational operator==(Rational R);
	Rational operator=(Rational R);
    friend ostream& operator<<(ostream& os,const Rational& a);

	int getNumerator() const;
	int getDenominator() const;
	int findGcd(int, int);
	void reduce();

};
//overload binary operators
Rational operator+(const Rational &R, const Rational &S);
Rational operator-(const Rational &R, const Rational &S);
Rational operator*(const Rational &R, const Rational &S);
Rational operator/(const Rational &R, const Rational &S);

#endif
