#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <cstdlib>

#include "Rational.h"
/** A class to represent a rational number
*/
using namespace std;

	//initialise as 1/1 when no arguments given
	Rational::Rational()
	{
		numerator = 1;
		denominator = 1;
	}

	Rational::Rational(int num, int denom)
	{
		numerator = num;
		denominator = denom;
	}

	//initialise whole number if only one argument given
	Rational::Rational(int num)
	{
		numerator = num;
		denominator = 1;
	}

	Rational::~Rational() {};

	int Rational::getNumerator() const
 	{
    	return numerator;
    }

    int Rational::getDenominator() const
 	{
    	return denominator;
    }

    //find the greatest common divisor of 2 rationals
    int Rational::findGcd(int a, int b) {
		if(b==0)
			return a;
		return findGcd(b, a%b);
	}
	//reduce the rational number if it can be
	void Rational::reduce() {
		int largest;
		//take the largest from the numerator and denominator
   		largest = numerator > denominator ? numerator : denominator;
   		//find gcd and divide both numerator and denominator by it
			//use the absolute value to ensure the sign is always stored in numerator
   		int gcd = abs(findGcd(numerator, denominator));
   		if (gcd != 0)
   		{
   			numerator /= gcd;
      		denominator /= gcd;
   		}
	}

	//print Rational number as numerator/denominator
	std::ostream& operator<<(std::ostream &strm, const Rational& a) {
 		return strm << a.getNumerator() << "/" << a.getDenominator();
	}

	Rational Rational::operator+=(Rational R)
	{
		//add numerator and denominator accordingly and reduce the rational number
    	numerator = (numerator * R.getDenominator()) + (R.getNumerator() * denominator);
    	denominator = (denominator * R.getDenominator());
    	(*this).reduce();
    	return (*this);
	}

	Rational Rational::operator-=(Rational R)
	{
		//substract the numerator and denominator accordingly and reduce the rational number
		numerator = (numerator * R.getDenominator()) - (R.getNumerator() * denominator);
		denominator = (denominator * R.getDenominator()) ;
		(*this).reduce();
		return *this;
	}

	Rational Rational::operator*=(Rational R)
	{
		//miltuply numerators and denominators respectively and reduce
		numerator = R.getNumerator() * numerator;
		denominator = R.getDenominator() * denominator;
		(*this).reduce();
		return *this;
	}

	Rational Rational::operator/=(Rational R)
	{
		if (R.getDenominator() == 0)
			throw std::overflow_error("Cannot divide by zero");
		//do the division and reduce
		numerator = numerator * R.getDenominator();
		denominator = denominator * R.getNumerator();
		(*this).reduce();
		return *this;
	}

	Rational operator*(const Rational &R, const Rational &S)
	{
		//use overloaded *= operator for multiplication
		Rational result = R;
		result *= S;
		return result;
	}

	Rational operator/(const Rational &R, const Rational &S)
	{
		//use overloaded /= operator for division
		Rational result = R;
		result /= S;
		return result;
	}

 	Rational operator+(const Rational &R, const Rational &S)
	{
		//use overloaded += operator for addition
		Rational result = R;
		result += S;
		return result;
	}

	Rational operator-(const Rational &R, const Rational &S)
	{
		//use overloaded += operator for substraction
		Rational result = R;
		result -= S;
		return result;
	}

	Rational Rational::operator=(Rational R)
	{
    	return *this;
	}
