#include <iostream>
#include "Rational.h"
using namespace std;

/** Test the Rational class with some examples
*/
	int main( int argc, char **argv) {

		Rational f1 = Rational(-3, 12);
		Rational f2 = Rational(4, 3);
		Rational f3 = Rational(0, 1);
		Rational result1 = f1 + f2;
		Rational result2 = f1 * f2;		
		Rational result3 = Rational(4) + f2;
		Rational result4 = f2 + Rational(5);
		Rational result5 = Rational(12) * f1;
		Rational result6 = f1 * Rational(6); 
		Rational result7 = f1 / f2;	

		cout<<"f1 = "<<f1<<endl;
		cout<<"f2 = "<<f2<<endl;
		cout<<"f3 = "<<f3<<endl;
		cout<<"f1 + f2 = "<<result1<<endl;
		cout<<"f1 * f2 = "<<result2<<endl;
		cout<<"4 + f2 = "<<result3<<endl;
		cout<<"f2 + 5 = "<<result4<<endl;
		cout<<"12 * f1 = "<<result5<<endl;
		cout<<"f1 * 6 = "<<result6<<endl;
		cout<<"f1 / f2 = "<<result7<<endl;
	}
