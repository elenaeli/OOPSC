#include <iostream>
#include "Quadrule_Functor.h"
#include "Integrator.h"
#include <chrono>

using namespace std;

int main()
{
    double result;
    DynQuad_Simp* dqs=new DynQuad_Simp();
    DynQuad_Trap* dqt=new DynQuad_Trap();
    DFunc* df=new DFunc();
    SFunctor sf=SFunctor();
    DqDfInt ddis(-3,13,100000,dqs);
    DqDfInt ddit(-3,13,100000,dqt);
    SqDfInt<StatQuad_Simp> sdis(-3,13,100000);
    SqDfInt<StatQuad_Trap> sdit(-3,13,100000);
    DqSfInt<SFunctor> dsis(-3,13,100000,dqs);
    DqSfInt<SFunctor> dsit(-3,13,100000,dqt);
    SqSfInt<StatQuad_Simp,SFunctor> ssis(-3,13,100000);
    SqSfInt<StatQuad_Trap,SFunctor> ssit(-3,13,100000);

    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=ddis(*df);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Dynamic simpsonrule and dynamic function: result:"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=dsis(sf);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Dynamic simpsonrule and static function: result:"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=sdis(*df);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Static simpsonrule and dynamic function: result:"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=ssis(sf);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Static simpsonrule and static function: result"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=ddit(*df);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Dynamic trapezoid and dynamic function: result:"<< result<< " time: "<<ms<<std::endl;
    }
        {
        const auto start = std::chrono::high_resolution_clock::now();
        result=dsit(sf);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Dynamic trapezoid and static function: result:"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=sdit(*df);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Static trapezoid and dynamic function: result:"<< result<< " time: "<<ms<<std::endl;
    }
    {
        const auto start = std::chrono::high_resolution_clock::now();
        result=ssit(sf);
        const auto end = std::chrono::high_resolution_clock::now();
        const auto ms = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count() / 1000.;
        std::cout<<"Static trapezoid and static function: result:"<< result<< " time: "<<ms<<std::endl;
    }
}
