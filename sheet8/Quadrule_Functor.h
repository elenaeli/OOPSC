#ifndef QUADRULE_FUNCTOR_H
#define QUADRULE_FUNCTOR_H


class DFunctor //Interface for a function like in the lecture
{
    public:
        virtual double operator () (const double& x) const = 0;
        virtual ~ DFunctor (){}
};


class DFunc : public DFunctor
{
    public:
        DFunc(){}
        double operator()(const double& x) const override  //calculates the value of the function for argument x
        {
            return 2*x*x+5;
        }

};
class SFunctor
{
    public:
        SFunctor(){}
        double operator()(const double& x) const  //calculates the value of the function for argument x
        {
            return 2*x*x+5;
        }

};

class DynQuad_Rule //interface for quadration rule
{
    public:
        virtual double calc(const double& a,const double& b,const DFunctor& f) const =0;
        virtual double calc(const double& a,const double& b,const SFunctor& f) const =0;
        virtual ~DynQuad_Rule(){}
};

class DynQuad_Trap : public DynQuad_Rule //tarapezoid rule
{
    public:
        DynQuad_Trap(){}
        virtual ~DynQuad_Trap(){}
        double calc(const double& a,const double& b, const DFunctor &f) const override
        {
            return ((b-a)/2)*(f(a)+f(b));
        }
        double calc(const double& a,const double& b, const SFunctor &f) const override
        {
            return ((b-a)/2)*(f(a)+f(b));
        }
};

class DynQuad_Simp : public DynQuad_Rule //simpson rule
{
    public:
        DynQuad_Simp(){}
        virtual ~DynQuad_Simp(){}
        double calc(const double& a,const double& b,const DFunctor& f) const override
        {
            return ((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b));
        }
        double calc(const double& a,const double& b,const SFunctor& f) const override
        {
            return ((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b));
        }
};

class StatQuad_Trap
{

    public:
        StatQuad_Trap(){}
        virtual ~StatQuad_Trap(){}
        double calc(const double& a,const double& b, const SFunctor &f) const
        {
            return ((b-a)/2)*(f(a)+f(b));
        }
        double calc(const double& a,const double& b, const DFunctor &f) const
        {
            return ((b-a)/2)*(f(a)+f(b));
        }
};


class StatQuad_Simp
{
    public:
        StatQuad_Simp(){}
        virtual ~StatQuad_Simp(){}
        double calc(const double& a,const double& b,const SFunctor& f) const
        {
            return ((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b));
        }
        double calc(const double& a,const double& b,const DFunctor& f) const
        {
            return ((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b));
        }
};
#endif // QUADRULE_FUNCTOR_H
