#ifndef INTEGRATOR_H
#define INTEGRATOR_H
#include "Quadrule_Functor.h"
class DqDfInt
{
    public:
        DqDfInt(const double& a, const double& b, const int& n, DynQuad_Rule*  qr) :a_(a),b_(b),n_(n),qr_(qr)
        {
            dist=(b-a)/n;
        }
        virtual ~DqDfInt(){}

        double operator()( DFunctor& f) const
        {
            double result=0;
            for (int i=0;i<n_;i++)
            {
                result+=qr_->calc(a_+(i*dist),a_+((i+1)*dist), f);
            }
            return result;
        }
    private:
        double a_,b_,dist;
        int n_;
        DynQuad_Rule* qr_;
};


template<typename T>
class SqDfInt
{
    public:
        SqDfInt(const double& a, const double& b, const int& n) :a_(a),b_(b),n_(n)
        {
            qr_=T();
            dist=(b-a)/n;
        }
        virtual ~SqDfInt(){}

        double operator()( DFunctor& f) const
        {
            double result=0;
            for (int i=0;i<n_;i++)
            {
                result+=qr_.calc(a_+i*dist,a_+(i+1)*dist, f);
            }
            return result;
        }
    private:
        double a_,b_,dist;
        int n_;
        T qr_;
};

template <typename Q>
class DqSfInt
{
    public:
        DqSfInt(const double& a, const double& b, const int& n, DynQuad_Rule*  qr) :a_(a),b_(b),n_(n),qr_(qr)
        {
            dist=(b-a)/n;
        }
        virtual ~DqSfInt(){}

        double operator()( Q& f) const
        {
            double result=0;
            for (int i=0;i<n_;i++)
            {
                result+=qr_->calc(a_+i*dist,a_+(i+1)*dist, f);
            }
            return result;
        }
    private:
        double a_,b_,dist;
        int n_;
        DynQuad_Rule* qr_;
};

template<typename T, typename Q>
class SqSfInt
{
    public:
        SqSfInt(const double& a, const double& b, const int& n) :a_(a),b_(b),n_(n)
        {
            T qr();
            dist=(b-a)/n;
        }
        virtual ~SqSfInt(){}

        double operator()( Q &f) const
        {
            double result=0;
            for (int i=0;i<n_;i++)
            {
                result+=qr_.calc(a_+i*dist,a_+(i+1)*dist, f);
            }
            return result;
        }
    private:
        double a_,b_,dist;
        int n_;
        T qr_;
};
#endif // INTEGRATOR_H
