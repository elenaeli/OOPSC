#include "MatrixClass.h"
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <complex>

// This file is empty, because by using templates we are forced to move all templated function implementations 
// to the header file.