#include "MatrixClass.h"
#include <iostream>
#include <cstdlib>
#include <string>
//#include "NumMatrixClass.h"
//#include "BaseMatrixClass.h"

int main()
{   
    std::cout << "Testing double as template of MatrixClass. " <<std::endl;

    // Define a matrix A1 of type double
    MatrixClass<double> A1(4,4);
    
    //std::cout<<r<<std::endl;
    std::cout << "A1.begin : " << A1.begin() << std::endl;

    // Define a square tridiagonal matrix A1
    for (int i=0;i<A1.Rows();++i)
      A1(i,i) = 2.0;
    for (int i=0;i<A1.Rows()-1;++i) 
      A1(i+1,i) = A1(i,i+1) = -1.0;
        A1.Print();

    // Define a matrix C1 of same content as A1
    MatrixClass<double> C1(A1);
    std::cout << "C1 : " <<std::endl;
    C1.Print();
    
    //A1 = 2*C1;
    std::cout << "A1 = 2 * C1" <<std::endl;
    A1.Print();
    
    //A1 = C1*2;
    std::cout << "A1 = C1 * 2." <<std::endl;
    A1.Print();
    
    //A1 = C1+A1;
    std::cout << "A1 = C1 + A1" <<std::endl;
    A1.Print();
    
    //Resize A1
    A1.Resize(5,5,2);
    for (int i=0;i<A1.Rows();++i)  
      A1(i,i) = 2.0;
    for (int i=0;i<A1.Rows()-1;++i) 
      A1(i+1,i) = A1(i,i+1) = -1.0;

    // Print A1
    std::cout << "A1 :" <<std::endl;
    A1.Print();

    
}