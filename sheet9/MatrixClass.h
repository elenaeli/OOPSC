#ifndef MATRIXCLASS_H
#define MATRIXCLASS_H
#include <iomanip>
#include <iostream>
#include <vector>
#include <cstdlib>

template<class T>
class MatrixClass
{
public:

	class ColIterator
	{
	public:
		ColIterator& operator++(); // move to next entry
		bool operator==(ColIterator&) const; // comparison of iterators
		T& operator*(); // access to current entry
		const T& operator*() const; // as above, but const
		unsigned int col() const; // number of current entry
		T& begin() const {
			return cols[1];
		};
		T& end() const {
			return cols[10];
		}
		ColIterator(int numRows, const double &value) : numRows_(numRows) { 
			for(int i = 0; i<numRows_;i++)
			{
				cols[i] = value;
			}
		}
	private:
		double cols[];    
		int numRows_;
			
	};
	template <typename R>
	class Row
	{
	private:
		std::vector<R> row;
		ColIterator colIt[];
		int numCols_;
	public:
		Row(int numCols, const T& value) : numCols_(numCols) {
			for (int j=0;j<numCols;++j)
			{ 
				row[j] = value;
				colIt[j] = ColIterator(row.size(), value);
			}
		};
		double operator [](int i) const    { return row[i]; }
		double & operator [](int i) { return row[i]; }
		friend std::ostream& operator<< ( std::ostream &os, const Row &r ) 
		{
			os << "[";
			for (int ii = 0; ii != r.row.size(); ++ii)
			{
				os << " " << r.row[ii];
			}
			os << "]";
			return os;
		}

		ColIterator& begin() const 
		{
			return colIt.begin();
		}
		ColIterator& end() const 
		{
			return colIt.end();
		}

	};
	template <typename R>
	class RowIterator
	{
	public:
		RowIterator<R>& operator++(); // move to next row
		bool operator==(RowIterator<R>&) const; // comparison of iterators
		Row<R>& operator*() ;
		const Row<R>& operator*() const; // as above, but const

		const Row<R>& operator[](int i) const
		{
			return rows[i];
		}
		unsigned int row() const;		
		RowIterator<R>(int numCols = 0, const T& value = T() ) : numCols_(numCols) {
			for (int r = 0; r < numCols_; r++)
			{ 
				rows[r] = Row<double>(numCols_,value);
			}
		};
		friend std::ostream& operator<< ( std::ostream &os, const RowIterator<R> &r ) 
		{
			os << "[";
			for (int ii = 0; ii != 10; ++ii)
			{
				os << " " << r.rows[ii];
			}
			os << "]";
			return os;
		}
		const Row<R>& begin() const 
		{			
			return rows[1];
		};
		const Row<R>& end() const 
		{
			return rows[a_.Rows()];
		}  
	private:
		int numCols_;
		Row<R> rows[];    
	};   
	// Set number of matrix rows and columns and initialize its elements with value
	void Resize(int numRows, int numCols, const T& value = T() );
	
	// Access matrix element at position (i,j)
	T& operator()(int i, int j);
	T  operator()(int i, int j) const;
 
	// Arithmetic functions 
	MatrixClass<T>& operator*=(T x);   
	MatrixClass<T>& operator+=(const MatrixClass<T>& x);
	
	// Output of matrix content
	void Print() const; 
		// Returns number of matrix rows
	int Rows() const
	{
		return numRows_;
	}
		// Returns number of matrix columns
	int Cols() const
	{
		return numCols_;
	}
	const Row<T>& begin() const
	{
		return rowIt.begin(); 
	}
	const Row<T>& end() const
	{
		return rowIt.end();
	}
	friend std::ostream& operator<<(std::ostream& os, const MatrixClass<T>& matrix)
	{
		for (int r = 0; r < matrix.Rows(); r++)
		{
			for (int c = 0; c < matrix.Cols(); c++)
			{
				std::cout << " " << matrix[r][c];
			}
			std::cout << std::endl;
		}
		return os;
	}
	// Constructor
	MatrixClass(int numRows = 0, int numCols = 0, const T& value = T()) :
		a_(numRows), numRows_(numRows), numCols_(numCols)
	{
		if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
		{
			numRows_ = 0;
			numCols_ = 0;
			a_.resize(numRows_);
		}
		else
		{
			for (int i=0;i<numRows_;++i)
				a_[i].resize(numCols_);
			numRows_ = numRows;
			numCols_ = numCols;     
		}
		rowIt = RowIterator<double>(numCols_, value);
		for (int i=0;i<numRows_;++i)
		{
			for (int j=0;j<numCols;++j)
				a_[i][j] = value;
		}
	}

	private:
		// matrix elements
		std::vector<std::vector<T> > a_;
		// number of rows
		int numRows_;
		// number of columns
		int numCols_;
		RowIterator<T> rowIt;
		Row<T> rows[]; 
};


	template <typename T>
	void MatrixClass<T>::Resize(int numRows, int numCols, const T& value)
	{
		a_.resize(numRows);
		for (int i=0;i<a_.size();++i)
		{
			a_[i].resize(numCols);
			for (int j=0;j<a_[i].size();++j)
				a_[i][j] = value;
		}
		numRows_ = numRows;
		numCols_ = numCols;
	}
	template <typename T>
	T& MatrixClass<T>::operator()(int i, int j)
	{
		if ((i<0)||(i>=numRows_))
		{
			std::cerr << "Illegal row index " << i;
			std::cerr << " valid range is (0:" << numRows_-1 << ")";
			std::cerr << std::endl;
			exit(EXIT_FAILURE);
		}
		if ((j<0)||(j>=numCols_))
		{
			std::cerr << "Illegal column index " << j;
			std::cerr << " valid range is (0:" << numCols_-1 << ")";
			std::cerr << std::endl;
			exit(EXIT_FAILURE);
		}
		return a_[i][j];
	}

	template <typename T>
	T MatrixClass<T>::operator()(int i,int j) const
	{
		if ((i<0)||(i>=numRows_))
		{
			std::cerr << "Illegal row index " << i;
			std::cerr << " valid range is (0:" << numRows_-1 << ")";
			std::cerr << std::endl;
			exit(EXIT_FAILURE);
		}
		if ((j<0)||(j>=numCols_))
		{
			std::cerr << "Illegal column index " << j;
			std::cerr << " valid range is (0:" << numCols_-1 << ")";
			std::cerr << std::endl;
			exit(EXIT_FAILURE);
		}
		return a_[i][j];
	}
	
	template <typename T>
	MatrixClass<T>& MatrixClass<T>::operator*=(T x)
	{
		for (int i=0;i<numRows_;++i)
			for (int j=0;j<numCols_;++j)
				a_[i][j]*=x;
				
		return *this;
	}
	template <typename T>
	MatrixClass<T>& MatrixClass<T>::operator+=(const MatrixClass<T>& x)
	{
		if ((x.numRows_!=numRows_)||(x.numCols_!=numCols_))
		{
			std::cerr << "Dimensions of matrix a (" << numRows_
								<< "x" << numCols_ << ") and matrix x (" 
								<< numRows_ << "x" << numCols_ << ") do not match!";
			exit(EXIT_FAILURE);
		}
		for (int i=0;i<numRows_;++i)
			for (int j=0;j<x.numCols_;++j)
				a_[i][j]+=x(i,j);
		return *this;
	}

	template <typename T>
	void MatrixClass<T>::Print() const
	{
		std::cout << "(" << numRows_ << "x";
		std::cout << numCols_ << ") matrix:" << std::endl;
		for (int i=0;i<numRows_;++i)
		{
			std::cout << std::setprecision(3);
			for (int j=0;j<numCols_;++j)
				std::cout << std::setw(5) << a_[i][j] << " ";
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
	
	// More arithmetic functions
	template <typename R, typename S>
	MatrixClass<R> operator*(const MatrixClass<R>& a, S x)
	{
		MatrixClass<R> temp(a);
		temp *= x;
		return temp;
	}
	template <typename S, typename R>
	MatrixClass<R> operator*(S x, const MatrixClass<R>& a)
	{
		MatrixClass<R> temp(a);
		temp *= x;
		return temp;
	}
	template <typename R>
	MatrixClass<R> operator+(const MatrixClass<R>& a,const MatrixClass<R>& b)
	{
		MatrixClass<R> temp(a);
		temp += b;
		return temp;
	}
#endif