#include <iostream>
#include <cstdio>
#include "Node.h"
#include <memory>

using namespace std;

	Node::Node() {
		next = NULL;
		previous = weak_ptr<Node>();
	}

	Node::Node(int value_) {
		value = value_;
		next = NULL;
		previous = weak_ptr<Node>();
	}

	Node::Node(int value_, shared_ptr<Node> next_) {
		value = value_;
		next = next_;
		previous = weak_ptr<Node>();
	}

	Node::Node(int value_, shared_ptr<Node> next_, shared_ptr<Node> previous_) {
		value = value_;
		next = next_;
		previous = previous_;
	}

	Node::~Node() {}

	int Node::getValue() {
		return value;
	}

	shared_ptr<Node> Node::getNext() const {
		return next;
	}

	shared_ptr<Node> Node::getPrevious() const {
		return previous.lock();
	}
