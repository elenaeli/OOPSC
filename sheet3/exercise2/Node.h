#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <memory>
using namespace std;

class Node {
friend class List;

public:
    Node();
    Node(int);
    Node(int, shared_ptr<Node>);
    Node(int, shared_ptr<Node>, shared_ptr<Node>);
    ~Node();
    shared_ptr<Node> getNext() const;
    shared_ptr<Node> getPrevious() const;
    int getValue();
    int value;

private:
	shared_ptr<Node> next;
  weak_ptr<Node> previous;

};

#endif
