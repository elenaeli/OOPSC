#ifndef LIST_H
#define LIST_H
#include "Node.h"
#include <memory>
using namespace std;
class List
{
public:
	List ();
	~List ();
	shared_ptr<Node> getFirst() const;
	shared_ptr<Node> getNext(shared_ptr<const Node>) const; 
	shared_ptr<Node> getPrevious(shared_ptr<const Node>) const; 
	void append (int);
	void insert (shared_ptr<const Node>, int );
	void erase (shared_ptr<const Node>);
	int value;

private:
	shared_ptr<Node> next;
	shared_ptr<Node> previous;
	shared_ptr<Node> first;
};

	void printList (const List&);
	void append (List&, const List& );

#endif