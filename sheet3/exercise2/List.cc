#include <iostream>
#include <cstdio>
#include "List.h"
#include "Node.h"
#include <memory>

using namespace std;

	List::List() {
		first = NULL;
		next = NULL;
	}

	List::~List() { }

	shared_ptr<Node> List::getFirst() const {
		return first;
	}
	//get the next node after a specified node
	shared_ptr<Node> List::getNext(shared_ptr<const Node> n) const {
		return n->next;
	}
	shared_ptr<Node> List::getPrevious(shared_ptr<const Node> n) const {
		return n->previous.lock();
	}


	//append a node with value i at the end of the list
	void List::append(int i) {
		shared_ptr<Node> newNode = make_shared<Node>(i);
		//if the list is empty, add the node at the beginning
	    if (!first) {
    	    first = newNode;
    	    first->previous = weak_ptr<Node>();
    	    return;
    	}
    	//otherwise the list is not empty
	    else
	    {
	    	shared_ptr<Node> current = first;
	    	//iterate until the end of the list and insert the node there
	    	while(current->next) {
	    		current = current->next;
			}
			current->next = newNode;
			newNode->previous = current;
		}
	}

	//insert a new node before a Node n
	void List::insert(shared_ptr<const Node> n, int i) {
		shared_ptr<Node> newNode = make_shared<Node>(i);
		//start from the first node
		shared_ptr<Node> previousNode = NULL;
		shared_ptr<Node> currentNode = first;
		//iterate until we find the node before which we want to insert
		while(currentNode != n) {
			previousNode = currentNode;
			currentNode = currentNode->next;
		}
		//if it is not the first node (there must be a previous node), insert it between previos and current node
		if(previousNode) {
			newNode->previous = previousNode;
			newNode->next = currentNode;
			previousNode->next = newNode;
		}
		//otherwise it is the first node, insert the node at the beginning
		else {
			newNode->next = currentNode;
			newNode->previous = weak_ptr<Node>();
			first = newNode;
			currentNode->previous = newNode;
		}
	}

	//remove a Node from a linked list
	void List::erase(shared_ptr<const Node> n) {
		shared_ptr<Node> previousNode = NULL;
		shared_ptr<Node> currentNode = NULL;
		//if it is the first node, remove it and change the first node to be the second node
		if(first->value == n->value) {
			currentNode = first;
			first = currentNode->next;
			first->previous = weak_ptr<Node>();
			currentNode.reset();
			return;
		}
		//start from the second node (previous is first node)
		previousNode = first;
		currentNode = first->next;
		//iterate over all nodes
		while(currentNode != NULL) {
			//if this is the node to be deleted, delete it and change the previous node next pointer
			if(currentNode->value == n->value) {
				previousNode->next = currentNode->next;
				//if it is not the last node, update the next node's previous pointer
				if(currentNode->next)
					(currentNode->next)->previous = previousNode;

				currentNode.reset();
				break;
			}
			//change the previous and current node to the next ones
			previousNode = currentNode;
			currentNode = currentNode->next;
		}
	}

	void printList (const List& list) {
		for ( shared_ptr<const Node> n = list.getFirst(); n != 0; n = list.getNext(n)) {
			std::cout << n->value << std::endl;
		}
	}


	void append(List& list1, const List& list2) {
		//for every Node in list2 just append it to list1.
		for ( shared_ptr<const Node> n = list2.getFirst(); n != 0; n = list2.getNext(n)) {
			list1.append(n->value);
		}
	}
