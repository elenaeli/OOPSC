In the second program v[0] is of type int* and bar() returns const int* (pointer to a constant int). 
We are trying to convert from a pointer to a constant int into a pointer to an int. This is clearly not allowed, because we will violate the constness.

However, in the first program it also gives us an error when we try to convert from an int** (pointer to a pointer 
to an int) into a const int**(pointer to a pointer to a constant int). The reason it is dangerous is that if it had worked
it would allow us to silently and accidentally change a const int object without a cast.
The right way would be to convert int** v to const int* const* w. So there isn't any chance you
could alter *w or **w. Without this form you could change **w and only *w is protected.