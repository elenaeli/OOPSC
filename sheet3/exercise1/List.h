#ifndef LIST_H
#define LIST_H
#include "Node.h"

class List
{
public:
	List ();
	~List ();
	Node* getFirst() const;
	Node* getNext(const Node*) const;
	void append (int);
	void insert (const Node*, int );
	void erase (const Node*);

	int value;

private:
	Node* next;
	Node* first;
};

	void printList (const List&);
	void append (List&, const List& );

#endif
