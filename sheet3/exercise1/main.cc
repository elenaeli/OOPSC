#include <iostream>
#include <cstdio>
#include "List.h"
#include "Node.h"
using namespace std;

int main ()
{
	List list;
	list.append(2);
	list.append(3);
	list.insert(list.getFirst(), 1);
	list.append(8);
	Node* nodeToErase = new Node(8);
	list.erase(nodeToErase);
	cout<<"list:"<<endl;
	printList(list);
	cout<<"\n";

	List list2;
	list2.append(4);
	list2.append(5);
	list2.append(6);
	append(list, list2);
	cout<<"list2:"<<endl;
	printList(list);
}
