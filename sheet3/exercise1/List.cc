#include <iostream>
#include <cstdio>
#include "List.h"
#include "Node.h"

using namespace std;

	List::List() {
		first = NULL;
		next = NULL;
	}

	List::~List() { }

	Node* List::getFirst() const {
		return first;
	}
	//get the next node after a specified node
	Node* List::getNext(const Node* n) const {
		return n->next;
	}

	//append a node with value i at the end of the list
	void List::append(int i) {
		Node *newNode = new Node(i);
		//if the list is empty, add the node at the beginning
	    if (!first) {
    	    first = newNode;
    	    return;
    	}
    	//otherwise the list is not empty
	    else
	    {
	    	Node *current = first;
	    	//iterate until the end of the list and insert the node there
	    	while(current->next) {
	    		current = current->next;
			}
			current->next = newNode;
		}
	}

	//insert a new node before a Node n
	void List::insert(const Node* n, int i) {
		Node *newNode = new Node(i);
		//start from the first node
		Node *previous = NULL;
		Node *current = first;
		//iterate until we find the node before which we want to insert
		while(current != n) {
			previous = current;
			current = current->next;
		}
		//if it is not the first node (there must be a previos node), insert it between previos and current node
		if(previous) {
			previous->next = newNode;
			newNode->next = current;
		}
		//otherwise it is the first node, insert the node at the beginning
		else {
			first = newNode;
			newNode->next = current;
		}
	}

	//remove a Node from a linked list
	void List::erase(const Node* n) {
		Node *previous = NULL;
		Node *current = NULL;
		//if it is the first node, remove it and change the first node to be the second node
		if(first->value == n->value) {
			current = first;
			first = current->next;
			delete current;
			return;
		}
		//start from the second node (previous is first node)
		previous = first;
		current = first->next;
		//iterate over all nodes
		while(current != NULL) {
			//if this is the node to be deleted, delete it and change the previos node next pointer
			if(current->value == n->value) {
				previous->next = current->next;
				delete current;
				break;
			}
			//change the previous and current node to the next ones
			previous = current;
			current = current->next;
		}
	}

	void printList (const List& list) {
		for (const Node* n = list.getFirst(); n != 0; n = list.getNext(n))
			std::cout << n->value << std::endl;
	}

	void append(List& list1, const List& list2) {
		//for every Node in list2 just append it to list1.
		for (const Node* n = list2.getFirst(); n != 0; n = list2.getNext(n))
		{
			list1.append(n->value);
		}
	}
