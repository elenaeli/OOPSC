#include "MatrixClass.h"
#include <iostream>
#include <cstdlib>
#include <complex>
#include "Rational.h"

int main()
{   
    std::cout << "Testing float as template of MatrixClass. " <<std::endl;
    // Define a matrix A1 of type float
    MatrixClass<float> A1(4,4);
    std::cout << "A1 : " <<std::endl;
    A1.Print();

    // Define a square tridiagonal matrix A1
    for (int i=0;i<A1.Rows();++i)  
      A1(i,i) = 2.0;
    for (int i=0;i<A1.Rows()-1;++i) 
      A1(i+1,i) = A1(i,i+1) = -1.0;

    // Define a matrix C1 of same content as A1
    MatrixClass<float> C1(A1);
    std::cout << "C1 : " <<std::endl;
    C1.Print();
    
    A1 = 2*C1;
    std::cout << "A1 = 2 * C1" <<std::endl;
    A1.Print();
    
    A1 = C1*2;
    std::cout << "A1 = C1 * 2." <<std::endl;
    A1.Print();
    
    A1 = C1+A1;
    std::cout << "A1 = C1 + A1" <<std::endl;
    A1.Print();
    
    //Resize A1
    A1.Resize(5,5,2);
    for (int i=0;i<A1.Rows();++i)  
      A1(i,i) = 2.0;
    for (int i=0;i<A1.Rows()-1;++i) 
      A1(i+1,i) = A1(i,i+1) = -1.0;

    // Print A1
    std::cout << "A1 :" <<std::endl;
    A1.Print();

    std::cout << "Testing std::complex<double> as template of MatrixClass. " <<std::endl;
    // Define a matrix A2 of type std::complex<double>
    MatrixClass< std::complex<double> > A2(4,4);
    std::cout << "A2 : " <<std::endl;
    A2.Print();

    // Define a square tridiagonal matrix A2
    for (int i=0;i<A2.Rows();++i)  
      A2(i,i) = 2.0;
    for (int i=0;i<A2.Rows()-1;++i) 
      A2(i+1,i) = A2(i,i+1) = -1.0;

    // Define a matrix C2 of same content as A2
    MatrixClass< std::complex<double> > C2(A2);
    std::cout << "C2 : " <<std::endl;
    C2.Print();
    
    A2 = 2*C2;
    std::cout << "A2 = 2 * C2" <<std::endl;
    A2.Print();
    
    A2 = C2*2;
    std::cout << "A2 = C2 * 2." <<std::endl;
    A2.Print();
    
    A2 = C2+A2;
    std::cout << "A2 = C2 + A2" <<std::endl;
    A2.Print();
    
    //Resize A2
    A2.Resize(5,5,2);
    for (int i=0;i<A2.Rows();++i)  
      A2(i,i) = 2.0;
    for (int i=0;i<A2.Rows()-1;++i) 
      A2(i+1,i) = A2(i,i+1) = -1.0;

    // Print A2
    std::cout << "A2 :" <<std::endl;
    A2.Print();

    std::cout << "Testing int as template of MatrixClass. " <<std::endl;
    // Define a matrix A3 of type int
    MatrixClass<int> A3(4,4);
    std::cout << "A3 : " <<std::endl;
    A3.Print();

    // Define a square tridiagonal matrix A3
    for (int i=0;i<A3.Rows();++i)  
      A3(i,i) = 2.0;
    for (int i=0;i<A3.Rows()-1;++i) 
      A3(i+1,i) = A3(i,i+1) = -1.0;

    // Define a matrix C3 of same content as A3
    MatrixClass< int > C3(A3);
    std::cout << "C3 : " <<std::endl;
    C3.Print();
    
    A3 = 2*C3;
    std::cout << "A3 = 2 * C3" <<std::endl;
    A3.Print();
    
    A3 = C3*2;
    std::cout << "A3 = C3 * 2." <<std::endl;
    A3.Print();
    
    A3 = C3+A3;
    std::cout << "A3 = C3 + A3" <<std::endl;
    A3.Print();
    
    //Resize A3
    A3.Resize(5,5,2);
    for (int i=0;i<A3.Rows();++i)  
      A3(i,i) = 2.0;
    for (int i=0;i<A3.Rows()-1;++i) 
      A3(i+1,i) = A3(i,i+1) = -1.0;

    // Print A3
    std::cout << "A3 :" <<std::endl;
    A3.Print();


    std::cout << "Testing Rational as template of MatrixClass. " <<std::endl;
    // Define a matrix A4 of type Rational
    MatrixClass<Rational> A4(4,4);
    std::cout << "A4 : " <<std::endl;
    A4.Print();

    // Define a square tridiagonal matrix A4
    for (int i=0;i<A4.Rows();++i)  
      A4(i,i) = 2.0;
    for (int i=0;i<A4.Rows()-1;++i) 
      A4(i+1,i) = A4(i,i+1) = -1.0;

    // Define a matrix C4 of same content as A4
    MatrixClass<Rational> C4(A4);
    std::cout << "C4 : " <<std::endl;
    C4.Print();
    
    A4 = 2*C4;
    std::cout << "A4 = 2 * C4" <<std::endl;
    A4.Print();
    
    A4 = C4*2;
    std::cout << "A4 = C4 * 2." <<std::endl;
    A4.Print();
    
    A4 = C4+A4;
    std::cout << "A4 = C4 + A4" <<std::endl;
    A4.Print();
    
    //Resize A4
    A4.Resize(5,5,2);
    for (int i=0;i<A4.Rows();++i)  
      A4(i,i) = 2.0;
    for (int i=0;i<A4.Rows()-1;++i) 
      A4(i+1,i) = A4(i,i+1) = -1.0;

    // Print A4
    std::cout << "A4 :" <<std::endl;
    A4.Print();
 
}