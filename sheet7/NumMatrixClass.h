#ifndef NUMMATRIX_H
#define NUMMATRIX_H
#include <iomanip>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "BaseMatrixClass.h"

template<class T>
class NumMatrixClass : public BaseMatrixClass<T>
{
public:
	using BaseMatrixClass<T>::BaseMatrixClass;
	NumMatrixClass<T>& operator*=(T x);   
    NumMatrixClass<T>& operator+=(const NumMatrixClass<T>& x);
    std::vector<std::vector<T> >  a() const
    {
        return BaseMatrixClass<T>::a_;     
    }
    int numRows() const
    {
    	return BaseMatrixClass<T>::numRows_;
    }
    int numCols() const
    {
    	return BaseMatrixClass<T>::numCols_;
    }
};
	template <typename T>
    NumMatrixClass<T>& NumMatrixClass<T>::operator*=(T x)
    {
      std::vector<std::vector<T> > aa = a();
      for (int i=0;i<numRows();++i)
        for (int j=0;j<numCols();++j)
          aa[i][j]*=x;
          
      return *this;
    }

    template <typename T>
    NumMatrixClass<T>& NumMatrixClass<T>::operator+=(const NumMatrixClass<T>& x)
    {
      if ((x.numRows()!=numRows())||(x.numCols()!=numCols()))
      {
        std::cerr << "Dimensions of matrix a (" << numRows()
                  << "x" << numCols() << ") and matrix x (" 
                  << numRows() << "x" << numCols() << ") do not match!";
        exit(EXIT_FAILURE);
      }
      std::vector<std::vector<T> > aa = a();
      for (int i=0;i<numRows();++i)
        for (int j=0;j<x.numCols();++j)
          aa[i][j]+=x(i,j);
      return *this;
  	}
	// More arithmetic functions
    template <typename R, typename S>
    NumMatrixClass<R> operator*(const NumMatrixClass<R>& a, S x)
    {
      NumMatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename S, typename R>
    NumMatrixClass<R> operator*(S x, const NumMatrixClass<R>& a)
    {
      NumMatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename R>
    NumMatrixClass<R> operator+(const NumMatrixClass<R>& a,const NumMatrixClass<R>& b)
    {
      NumMatrixClass<R> temp(a);
      temp += b;
      return temp;
    }
#endif