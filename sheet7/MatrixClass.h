#ifndef MATRIX_H
#define MATRIX_H
#include <iomanip>
#include <iostream>
#include <vector>
#include <cstdlib>
/**
 * Matrix Class representing matrices of double values
 */
template<class T>
class MatrixClass
{
  public:
    // Set number of matrix rows and columns and initialize its elements with value
    void Resize(int numRows, int numCols, const T& value = T() );
    
    // Access matrix element at position (i,j)
    T& operator()(int i, int j);
    T  operator()(int i, int j) const;
   
    // Arithmetic functions 
    MatrixClass<T>& operator*=(T x);   
    MatrixClass<T>& operator+=(const MatrixClass<T>& x);
    
    // Output of matrix content
    void Print() const; 

    // Returns number of matrix rows
    int Rows() const
    {
      return numRows_;
    }

    // Returns number of matrix columns
    int Cols() const
    {
      return numCols_;
    }

    // Constructor
    MatrixClass(int numRows = 0, int numCols = 0, const T& value = T() ) :
      a_(numRows), numRows_(numRows), numCols_(numCols)
    {
    if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
    {
      numRows_ = 0;
      numCols_ = 0;
      a_.resize(numRows_);
    }
    else
    {
      for (int i=0;i<numRows_;++i)
        a_[i].resize(numCols_);
      numRows_ = numRows;
      numCols_ = numCols;	    
    }
    for (int i=0;i<numRows;++i)
    {
      for (int j=0;j<numCols;++j)
        a_[i][j] = value;
    }
  }

  private:
    // matrix elements
    std::vector<std::vector<T> > a_;
    // number of rows
    int numRows_;
    // number of columns
    int numCols_;
};

    template <typename T>
    void MatrixClass<T>::Resize(int numRows, int numCols, const T& value)
    {
      a_.resize(numRows);
      for (int i=0;i<a_.size();++i)
      {
        a_[i].resize(numCols);
        for (int j=0;j<a_[i].size();++j)
          a_[i][j] = value;
      }
      numRows_ = numRows;
      numCols_ = numCols;
    }

    template <typename T>
    T& MatrixClass<T>::operator()(int i, int j)
    {
      if ((i<0)||(i>=numRows_))
      {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      if ((j<0)||(j>=numCols_))
      {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      return a_[i][j];
    }

    template <typename T>
    T MatrixClass<T>::operator()(int i,int j) const
    {
      if ((i<0)||(i>=numRows_))
      {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      if ((j<0)||(j>=numCols_))
      {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      return a_[i][j];
    }
    
    template <typename T>
    MatrixClass<T>& MatrixClass<T>::operator*=(T x)
    {
      for (int i=0;i<numRows_;++i)
        for (int j=0;j<numCols_;++j)
          a_[i][j]*=x;
          
      return *this;
    }

    template <typename T>
    MatrixClass<T>& MatrixClass<T>::operator+=(const MatrixClass<T>& x)
    {
      if ((x.numRows_!=numRows_)||(x.numCols_!=numCols_))
      {
        std::cerr << "Dimensions of matrix a (" << numRows_
                  << "x" << numCols_ << ") and matrix x (" 
                  << numRows_ << "x" << numCols_ << ") do not match!";
        exit(EXIT_FAILURE);
      }
      for (int i=0;i<numRows_;++i)
        for (int j=0;j<x.numCols_;++j)
          a_[i][j]+=x(i,j);
      return *this;
    }

    template <typename T>
    void MatrixClass<T>::Print() const
    {
      std::cout << "(" << numRows_ << "x";
      std::cout << numCols_ << ") matrix:" << std::endl;
      for (int i=0;i<numRows_;++i)
      {
        std::cout << std::setprecision(3);
        for (int j=0;j<numCols_;++j)
            std::cout << std::setw(5) << a_[i][j] << " ";
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }

    // More arithmetic functions
    template <typename R, typename S>
    MatrixClass<R> operator*(const MatrixClass<R>& a, S x)
    {
      MatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename S, typename R>
    MatrixClass<R> operator*(S x, const MatrixClass<R>& a)
    {
      MatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename R>
    MatrixClass<R> operator+(const MatrixClass<R>& a,const MatrixClass<R>& b)
    {
      MatrixClass<R> temp(a);
      temp += b;
      return temp;
    }

#endif