#ifndef OUTOFBOUNDSEXCEPTION_H
#define OUTOFBOUNDSEXCEPTION_H
#include <iostream>
#include <string>
#include <exception>

class OutOfBoundsException : public std::exception
{
  private:
    std::string errorMsg;

  public:
    OutOfBoundsException(const char *msg);
    virtual ~OutOfBoundsException() throw();   
    virtual const char* what() const throw() ;
};

#endif