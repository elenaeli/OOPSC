#include <iostream>
#include <string>
#include <exception>
#include "VectorSizeException.h"

VectorSizeException::VectorSizeException(const char *msg) : errorMsg(msg) {};
    
VectorSizeException::~VectorSizeException() throw() {};
   
const char* VectorSizeException::what() const throw() { 
    return errorMsg.c_str();
};
