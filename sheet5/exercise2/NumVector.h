#ifndef NUMVECTOR_H
#define NUMVECTOR_H
#include <vector>

class NumVector 
{
public:
	NumVector(int );
	virtual ~NumVector();
	double norm() const;
	double& operator[](const int);
	double operator* (const NumVector v2);
	const double &operator[](const int i) const;
	int size() const;
	
private:
	std::vector<double> v;
};
   
#endif