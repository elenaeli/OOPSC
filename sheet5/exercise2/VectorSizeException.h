#ifndef VECTORSIZEEXCEPTION_H
#define VECTORSIZEEXCEPTION_H
#include <iostream>
#include <string>
#include <exception>

class VectorSizeException : public std::exception
{
  private:
    std::string errorMsg;

  public:
    VectorSizeException(const char *msg);
    virtual ~VectorSizeException() throw();   
    virtual const char* what() const throw() ;
};

#endif