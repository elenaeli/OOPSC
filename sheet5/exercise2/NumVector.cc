#include <vector>
#include <iostream>
#include <cmath> 
#include <string> 
#include <sstream>
#include "OutOfBoundsException.h"
#include "VectorSizeException.h"
#include "NumVector.h"

NumVector::NumVector(int size) {
	v = std::vector<double>(size); 
}

NumVector::~NumVector() {	
}

double NumVector::norm() const
{
    double result = 0.0;
    for(int i = 0; i < v.size(); i++) {
        result += (v[i] * v[i]);
    }
    return sqrt(result);
}

double& NumVector::operator[] (const int i)
{
	if(i>=0 && i<v.size())
    	return v[i];
    else {
        std::string errorMsg = "Array index " + std::to_string(i) + " is out of bounds." ;
    	throw OutOfBoundsException(errorMsg.c_str());
    }
}

double NumVector::operator* (const NumVector v2)
{
    if(v.size() != v2.size()) {
        std::string errorMsg = "Vector sizes do not match. First vector is of size " + std::to_string(v.size()) 
            + " and second vector is of size " + std::to_string(v2.size()) + ".";
        throw VectorSizeException(errorMsg.c_str());
    }
    else {
        double result = 0.0;
        for(int i=0; i<v.size(); i++)
            result += v[i] * v2[i];
        return result;
    }
}
const double& NumVector::operator[](const int i) const
{
	if(i>=0 && i<v.size())
        return v[i];
    else
        throw OutOfBoundsException("Array index is out of bounds");
}

int NumVector::size() const
{
	return v.size();
}
