#include <iostream>
#include <string>
#include <exception>
#include "OutOfBoundsException.h"

OutOfBoundsException::OutOfBoundsException(const char *msg) : errorMsg(msg) {};
    
OutOfBoundsException::~OutOfBoundsException() throw() {};
   
const char* OutOfBoundsException::what() const throw() { 
    return errorMsg.c_str();
};
