#include <vector>
#include <iostream>
#include <cmath>
#include "NumVector.h"
#include "OutOfBoundsException.h"
#include "VectorSizeException.h"

using namespace std;

int main() {
	NumVector v1(3);
	NumVector v2(3);
	NumVector v4(4);
	NumVector v3(3);

	v1[0]=1; v1[1]=3; v1[2]=4;
	v2[0]=2; v2[1]=5; v2[2]=10;
	v3[0]=6; v3[1]=7; v3[2]=8;

	v4[0]=3; v4[1]=4; v4[2]=5; v4[3]=6;

	try {
		cout << "Vector v1: [" << v1[0] << ", " << v1[1] << ", " << v1[2] << "]" << endl;
		cout << "Vector v2: [" << v2[0] << ", " << v2[1] << ", " << v2[2] << "]" << endl;
		cout << "Vector v3: [" << v3[0] << ", " << v3[1] << ", " << v3[2] << "]" << endl;
		cout << "Vector v4: [" << v4[0] << ", " << v4[1] << ", " << v4[2] << ", " 
			<< v4[3] << "]" << endl;
		cout << "v4[4]: " << endl;
		cout << v4[4] << endl;

	}
	catch(OutOfBoundsException e) {
		cerr << "ERROR: " << e.what() << endl;
	}

	try {
		cout << "v1 * v2 = " << v1*v2 << endl;
		cout << "v1 * v3 = " << v1*v3 << endl;
		cout << "v2 * v4 = " << endl;
		cout << v2*v4 << endl;
	}
	catch(VectorSizeException e) {
		cerr << "ERROR: " << e.what() << endl;
	}

}
