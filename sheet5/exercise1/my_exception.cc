#include <iostream>
#include <string>
#include <exception>
#include "my_exception.h"

my_exception::my_exception(const char *msg) : errorMsg(msg) {};
    
my_exception::~my_exception() throw() {};
   
const char* my_exception::what() const throw() { 
    	return errorMsg.c_str();
};
