#ifndef MYEXCEPTION_H
#define MYEXCEPTION_H
#include <iostream>
#include <string>
#include <exception>

class my_exception : public std::exception 
{
  private:
    std::string errorMsg;

  public:
    my_exception(const char *msg);
    ~my_exception() throw();   
    const char* what() const throw();
};
#endif