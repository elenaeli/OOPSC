Compile with:
g++ main.cc

This program makes a Parent and a Child play Ball, but it isn't valid because it builds a new block at every throw and at some time the program needs to abort. The program cannot stop without errors, since the catch clause makes it execute the aim() method again and again. This way there is an infinite recursion, which cannot stop. That is why it gives a Segmentation fault and valgrind gives as output:

==6639== Stack overflow in thread 1: can't grow stack to 0xffe801ff8
==6639== Process terminating with default action of signal 11 (SIGSEGV)
==6639==  Access not within mapped region at address 0xFFE801FF8
==6639==    at 0x514C0E6: ??? (in /lib/x86_64-linux-gnu/libgcc_s.so.1)
