#include <iostream>
class P;
class Ball : public std::exception {};
class P
{
public:
    P* Target;
    P (P* Target) {
        this->Target = Target;
    }
    void aim(Ball ball) {
        try {
            throw ball;
        }
        catch(Ball B) {
            Target->aim(B);
        }
    }
};

int main()
{
    P* Parent = new P(NULL); //creates new object of type P named Parent
    P* Child = new P(Parent);  //creates new object of type P named Child and sets Parent as the Target of Child
    Parent->Target = Child;  //sets Child as the Target of the Parent
    Parent->aim(Ball()); //Parent uses aim, this throws the exception ball, this gets catched and the Child uses aim and throws ball etc.
    return 0;
}
