#include <iostream>

template<int n>
class FibonacciTMP
{
public:
    static const unsigned long long value = FibonacciTMP<n-1>::value + FibonacciTMP<n-2>::value;
    static unsigned long long getValue(int i)
    {
        if (i == n) {
            return value;
        } 
        else {
            return FibonacciTMP<n-1>::getValue(i); 
        }
    }
};

template<>
class FibonacciTMP<0>
{
public:
    static const unsigned long long value = 1;
    static int getValue(int i)
    {
        return value;
    }
};

template<> 
class FibonacciTMP<1>
{
public:
    static const unsigned long long value = 1;
    static int getValue(int i)
    {        
        return value;
    }
};
