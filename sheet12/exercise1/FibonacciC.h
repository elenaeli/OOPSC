#include <iostream>

template<int n>
class FibonacciC
{
public:
 	static constexpr unsigned long long value = FibonacciC<n-1>::value + FibonacciC<n-2>::value;
 	static unsigned long long getValue(int i)
    {
        if (i == n) {
            return value;
        } 
        else {
            return FibonacciC<n-1>::getValue(i); 
        }
    }
};
 
template<>
class FibonacciC<0>
{
public:
 	static constexpr unsigned long long value = 1;
 	static unsigned long long getValue(int i)
    {
        return value;
    }
};
 
template<>
class FibonacciC<1>
{
public:
	static constexpr unsigned long long value = 1;
	static unsigned long long getValue(int i)
    {
        return value;
    }
};