#include <iostream>
#include <ctime>
#include <chrono>
#include "FibonacciTMP.h"
#include "FibonacciC.h"

unsigned long long fibonacci(int n) {
    if (n == 1 || n == 0) {
        return 1;
    } else {
        return fibonacci(n-1)+fibonacci(n-2);
    }
}

int main(int, char *[])
{
    // compute first version (template metaprogramming) with n=100
    auto begin1 = std::chrono::high_resolution_clock::now();
    FibonacciTMP<100> f1;
    std::cout<< "Fibonacci with template metaprogramming" << std::endl;
    for (int i = 0; i < 100; ++i) {
        std::cout << "Fibonnaci " << i << " = " << f1.getValue(i) << std::endl;
    }
    std::cout << std::endl;
    auto end1 = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds> (end1-begin1).count() << " micro seconds" << std::endl;

    //compute second version (with constexpression) with n=100
    auto begin2 = std::chrono::high_resolution_clock::now();
    std::cout << "Fibonacci with constexpr" << std::endl;
    FibonacciC<100> f2;
    for (int i = 0; i < 100; ++i) {
        std::cout << "Fibonacci " << i << " = " << f2.getValue(i) << std::endl;
    }
    std::cout << std::endl;
    auto end2 = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds> (end2-begin2).count() << " micro seconds" << std::endl;
    
    // for the recursion only compute up to 40 because wih n > 40 it is very slow
    auto begin3 = std::chrono::high_resolution_clock::now();
    std::cout << "Fibonacci with recursion" << std::endl;
    for (int i = 0; i < 40; ++i) {
        std::cout << "Fibonacci " << i << " = " << fibonacci(i) << std::endl;
    }
    auto end3= std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds> (end3-begin3).count() << " micro seconds" << std::endl;

}