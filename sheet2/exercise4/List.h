#ifndef LIST_H
#define LIST_H

#include "Node.h"

class List
{
public:
	List ();
	~List ();
	Node* getFirst();
	Node* getNext(const Node*); 
	void append (int);
	void insert (Node*, int );
	void erase (Node*);
	int value;

private:
	Node* next;
	Node* first;
};

#endif