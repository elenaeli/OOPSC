#include <iostream>
#include <cstdio>
#include "Node.h"

using namespace std;

	Node::Node() {
		next = NULL;
	}

	Node::Node(int value_) {
		value = value_;
		next = NULL;
	}

	Node::Node(int value_, Node* next_) {
		value = value_;
		next = next_;
	}

	Node::~Node() {}

	int Node::getValue() {
		return value;
	}

	Node* Node::getNext() {
		return next;
	}