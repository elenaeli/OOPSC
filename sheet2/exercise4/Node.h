#ifndef NODE_H
#define NODE_H
#include <iostream>

class Node {
friend class List;

public:
    Node();
    Node(int);
    Node(int, Node*);
    ~Node();
    Node* getNext();
    int getValue();
    int value;

private:
    Node* next;
};

#endif
