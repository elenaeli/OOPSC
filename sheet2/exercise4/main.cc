#include <iostream>
#include <cstdio>
#include "List.h"
#include "Node.h"
using namespace std;

int main ()
{
	List list;
	list.append(2);
	list.append(3);
	list.insert(list.getFirst(), 1);
	list.append(8);
	Node* nodeToErase = new Node(8);
	list.erase(nodeToErase);
	for (Node* n = list.getFirst(); n != 0; n = list.getNext(n))
		cout << n->value << endl;	
	return 0;
}