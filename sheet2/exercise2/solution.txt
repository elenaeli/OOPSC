The destructor of a class C is accountable for...
-–… cleaning up all objects off the class C on the heap (that is clear)

The destructor of a class C is not accountable for...
-–cleaning up components of objects of the class C that are on the heap (This point is only right when the object is on the heap. In this case the destructor deletes the class which calls the destructor of the class, which then deletes this objects. Therefore it is technically partial the responsibility of the destructor of C. But if the object is on the stack the destructor gets called automatically.
-– cleaning up all objects of the class C. (This is wrong, because it is only responible for the objects on the heap)
-–delete is just a special way of calling the destructor: let x be of type C*, then delete x; is the same as (*x).~C() (That is wrong, because the space on the heap for C is still allocated)
