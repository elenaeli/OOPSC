#ifndef COMP_INT_H
#define COMP_INT_H
#include "Quad_Rule.h"

class Integrator //most general class, defines only the property to integrate between two numbers a and b
{
    public:
        Integrator(){}
        virtual double operator () ( Functor& f) const = 0;  // operator for integrating
        virtual ~ Integrator (){}
};

class Comp_Int : public Integrator //less general, should have a fixed quadratic rule, choice of intervals is missing
{
        public:
            Comp_Int(){}
            virtual double operator () ( Functor& f) const = 0; //operator for integrating
            virtual ~ Comp_Int(){}
};

class Eqd_Comp_Int: public Comp_Int // fixed instnce, quadration rule is given and equidstant intervals
{
    public:
        Eqd_Comp_Int(const double& a, const double& b, const int& n, Quad_Rule* const qr) :a_(a),b_(b),n_(n),qr_(qr)//interval, steps and quadration rule fixed
        {
            dist=(b-a)/n;
        }
        virtual ~Eqd_Comp_Int(){}

        double operator()( Functor& f) const override  //calculate the integral with the given parameters
        {
            double result=0;
            for (int i=0;i<n_;i++)
            {
                result+=qr_->calc(a_+i*dist,b_+(i+1)*dist, f);
            }
            return result;
        }
    private:
        double a_,b_,dist;
        int n_;
        Quad_Rule* qr_;
};

#endif 