#include <iostream>
#include "Comp_Int.h"
#include "Functor.h"
#include "Quad_Rule.h"
#include <cmath>

using namespace std;
const int STEP_NUMBER=50;
int main()
{
    Quad_Rule* qt = new Quad_Trap();
    Quad_Rule* qs = new Quad_Simp();
    Integrator* ecit1= new Eqd_Comp_Int(-3,13,STEP_NUMBER,qt);
    Integrator* ecis1= new Eqd_Comp_Int (-3,13,STEP_NUMBER,qs);
    Integrator* ecit2= new Eqd_Comp_Int(0,2*M_PI,STEP_NUMBER,qt);
    Integrator* ecis2= new Eqd_Comp_Int (0,2*M_PI,STEP_NUMBER,qs);
    Func1 f1;
    Func2 f2;
    cout << "Integral of function one with Tarpezoid-Rule:" <<  (*ecit1)(f1)<<endl; //exactly as on slide 23, but doesn't work
    cout << "Integral of function one with Simpson-Rule:" <<  (*ecis1)(f1)<<endl;
    cout << "Integral of function two with Tarpezoid-Rule:" <<  (*ecit2)(f2)<<endl;
    cout << "Integral of function two with Simpson-Rule:" <<  (*ecis2)(f2)<<endl;;
    f1.integrationInterval (-3,13);
    f2.integrationInterval (0,2*M_PI);
    cout << "exact integral of function one:" << f1.exactIntegral()<<endl;
    cout << "exact integral of function two:" << f2.exactIntegral()<<endl;
    delete qt;
    delete qs;
}
