#ifndef QUAD_RULE_H
#define QUAD_RULE_H
#include "Functor.h"



class Quad_Rule //interface for quadration rule
{
    public:
        virtual double calc(const double& a,const double& b,const Functor& f) const =0;
        virtual ~Quad_Rule(){}
};

class Quad_Trap : public Quad_Rule //tarapezoid rule
{
    public:
        Quad_Trap(){}
        virtual ~Quad_Trap(){}
        double calc(const double& a,const double& b, const Functor &f) const override
        {
            return ((b-a)/2)*(f(a)+f(b));
        }
};

class Quad_Simp : public Quad_Rule //simpson rule
{
    public:
        Quad_Simp(){}
        virtual ~Quad_Simp(){}
        double calc(const double& a,const double& b,const Functor& f) const override
        {
            return ((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b));
        }
};
#endif // QUAD_RULE_H
