#ifndef FUNCTOR_H
#define FUNCTOR_H
#include <cmath>

class Functor //Interface for a function like in the lecture
{
    public:
        virtual double operator () (const double& x) const = 0;
        virtual ~ Functor (){}
};
class Func1 : public Functor // First given function for testing
{
    public:
        Func1(){}
        double operator()(const double& x) const override  //calculates the value of the function for argument x
        {
            return 2*x*x+5;
        }
        void integrationInterval(const double& l, const double& r) 
        {
            left=l;
            right=r;
        }
        virtual ~Func1(){}

        double exactIntegral() const
        {
            return (2/3)*(right*right*right+5*right-left*left*left+5*left);
        }
    private:
        double left;
        double right;
};


class Func2 : public Functor //Second function for testing
{
    public:
        Func2(){}
        double operator()(const double& x) const override //calculates the value of the function for argument x
        {
            return (x/M_PI)*sin(x);
        }
        virtual ~Func2(){}
        void integrationInterval(const double& l, const double& r)
        {
            left=l;
            right=r;
        }
        double exactIntegral() const
        {
            return (1/M_PI)*(sin(right)-right*cos(right)-sin(left)+cos(left));
        }
    private:
        double left;
        double right;
};
#endif // FUNCTOR_H
