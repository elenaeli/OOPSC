#include <vector>
#include <iostream>
#include <cmath>
#include <numeric>
#include <cstdlib>
#include <iomanip>      // std::setprecision
#include "MatrixClass.h"

using namespace std;

int main() {
    
    // EXERCISE 1
    MatrixClass<int> A1(3,3);
    A1(0,0) = 5; A1(0,1) = 3; A1(0,2) = 4;
    A1(1,0) = 2; A1(1,1) = 3; A1(1,2) = 2;
    A1(2,0) = 3; A1(2,1) = 6; A1(2,2) = 1;    
    std::cout << "Matrix A1 of int = " << std::endl ;
    A1.Print();
    std::cout << "Frobenius norm of A1 = " << std::fixed << sqrt(A1.frobeniusnorm()) << std::endl << std::endl ;

    MatrixClass<float> A2(3, 3);
    A2(0,0) = 2.51; A2(0,1) = 3.42; A2(0,2) = 1.84;
    A2(1,0) = 6.46; A2(1,1) = 1.37; A2(1,2) = 3.91;
    A2(2,0) = 1.84; A2(2,1) = 5.91; A2(2,2) = 2.51;
    std::cout << "Matrix A2 of float = " << std::endl ;
    A2.Print();
    std::cout << "Frobenius norm of A2 = " << std::fixed << sqrt(A2.frobeniusnorm()) << std::endl << std::endl;

    MatrixClass<double> A3(3, 3);
    A3(0,0) = 3.45; A3(0,1) = 2.21; A3(0,2) = 3.96;
    A3(1,0) = 1.58; A3(1,1) = 3.45; A3(1,2) = 2.74;
    A3(2,0) = 2.21; A3(2,1) = 3.96; A3(2,2) = 1.58;
    std::cout << "Matrix A3 of double = " << std::endl ;
    A3.Print();
    std::cout << "Frobenius norm of A3 = " << std::fixed << sqrt(A3.frobeniusnorm()) << std::endl << std::endl;

    MatrixClass<Rational> A4(3, 3);
    A4(0,0) = Rational(1,3); A4(0,1) = Rational(3,4); A4(0,2) = Rational(2,3);
    A4(1,0) = Rational(1,4); A4(1,1) = Rational(2,5); A4(1,2) = Rational(4,5);
    A4(2,0) = Rational(2,3); A4(2,1) = Rational(3,5); A4(2,2) = Rational(3,4);
    std::cout << "Matrix A4 Rational = " << std::endl;
    A4.Print();
    std::cout << "Frobenius norm of A4 = " << std::fixed << sqrt(double(A4.frobeniusnorm().getNumerator()) / double(A4.frobeniusnorm().getDenominator())) << std::endl << std::endl;

    MatrixClass<complex<int>> A5(3, 3);
    A5(0,0) = complex<int>(2,3); A5(0,1) = complex<int>(5,3); A5(0,2) = complex<int>(4,8);
    A5(1,0) = complex<int>(6,1); A5(1,1) = complex<int>(8,2); A5(1,2) = complex<int>(2,3);
    A5(2,0) = complex<int>(5,3); A5(2,1) = complex<int>(6,1); A5(2,2) = complex<int>(8,2);
    std::cout << "Matrix A5 of complex<int> = " << std::endl ;
    A5.Print();
    std::cout << "Frobenius norm of A5 = " << std::fixed << sqrt(A5.frobeniusnorm()) << std::endl << std::endl;

    MatrixClass<complex<float>> A6(3, 3);
    A6(0,0) = complex<float>(4.6,2); A6(0,1) = complex<float>(9.21,3); A6(0,2) = complex<float>(0.48,7);
    A6(1,0) = complex<float>(5.3,6); A6(1,1) = complex<float>(6.3,4); A6(1,2) = complex<float>(9.21,3);
    A6(2,0) = complex<float>(2.3,5); A6(2,1) = complex<float>(0.48,7); A6(2,2) = complex<float>(4.6,2);
    std::cout << "Matrix A6 of complex<float> = " << std::endl ;
    A6.Print();
    std::cout << "Frobenius norm of A6 = " << std::fixed << sqrt(A6.frobeniusnorm()) << std::endl << std::endl;

    MatrixClass<complex<double>> A7(3, 3);
    A7(0,0) = complex<double>(2.78,2); A7(0,1) = complex<double>(4.66,3); A7(0,2) = complex<double>(4.95,5);
    A7(1,0) = complex<double>(4.95,5); A7(1,1) = complex<double>(3.12,6); A7(1,2) = complex<double>(2.78,2);
    A7(2,0) = complex<double>(9.21,3); A7(2,1) = complex<double>(9.21,6); A7(2,2) = complex<double>(4.66,3);
    std::cout << "Matrix A7 of complex<double> = " << std::setprecision(3) << std::endl ;
    A7.Print();
    std::cout << "Frobenius norm of A7 = " << std::fixed << sqrt(A7.frobeniusnorm()) << std::endl << std::endl;


    // EXERCISE 2 
    std::cout << "Policies: " << std::endl;
    MatrixClass<int> B(3,3);
    B(0,0) = 2; B(0,1) = 1; B(0,2) = 0;
    B(1,0) = 1; B(1,1) = 3; B(1,2) = 2;
    B(2,0) = 0; B(2,1) = 1; B(2,2) = -4;
    
    std::cout << "Matrix B = " << std::endl;
    B.Print();

    MatrixTraits<int>::MatrixType frobNorm = matrixNorm<int, FrobeniusPolicy>(B);
    MatrixTraits<int>::MatrixType rowSumNorm = matrixNorm<int, RowSumPolicy>(B);
    MatrixTraits<int>::MatrixType totalNorm = matrixNorm<int, TotalPolicy>(B);
    std::cout << "Frobenius Norm Policy: " << sqrt(frobNorm) << std::endl;
    std::cout << "Row Sum Norm Policy: " << rowSumNorm<< std::endl;
    std::cout << "Total Norm Policy: " << B.Rows() * totalNorm<< std::endl;
}
