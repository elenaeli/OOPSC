#ifndef MATRIX_H
#define MATRIX_H
#include <iomanip>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <cmath>
#include "Interface.h"
#include "Rational.h"
#include "NormPolicy.h"
#include <complex>

/**
 * Matrix Class representing matrices of double values
 */
template<class T>
class MatrixClass : public MatrixInterface<T>
{
  public:
    // Set number of matrix rows and columns and initialize its elements with value
    void Resize(int numRows, int numCols, const T& value = T() );
    
    // Access matrix element at position (i,j)
    T& operator()(int i, int j);
    T  operator()(int i, int j) const;
   
    // Arithmetic functions 
    MatrixInterface<T>& operator*=(T x);   
    MatrixInterface<T>& operator+=(const  MatrixInterface<T>& x);
    
    // Output of matrix content
    void Print() const; 
    typename MatrixTraits<T>::MatrixType frobeniusnorm() const;
        
    // Returns number of matrix rows
    int Rows() const
    {
      return numRows_;
    }

    // Returns number of matrix columns
    int Cols() const
    {
      return numCols_;
    }

    // Constructor
    MatrixClass(int numRows = 0, int numCols = 0, const T& value = T() ) :
      a_(numRows), numRows_(numRows), numCols_(numCols)
    {
    if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
    {
      numRows_ = 0;
      numCols_ = 0;
      a_.resize(numRows_);
    }
    else
    {
      for (int i=0;i<numRows_;++i)
        a_[i].resize(numCols_);
      numRows_ = numRows;
      numCols_ = numCols;	    
    }
    for (int i=0;i<numRows;++i)
    {
      for (int j=0;j<numCols;++j)
        a_[i][j] = value;
    }
  }

  private:
    // matrix elements
    std::vector<std::vector<T> > a_;
    // number of rows
    int numRows_;
    // number of columns
    int numCols_;
};

    template<typename T>
    T l2Norm(const std::vector<T> &v) {
        return std::accumulate(v.begin(), v.end(), static_cast<T>(0),
            [](T sum, T el) { return sum + el * el; });
    }
    template<typename T>
    T l1Norm(const std::vector<T>& v) {
        auto sum = [](T temp, T val) {
            return temp + abs(val); 
        }; 
        return std::accumulate(v.begin(), v.end(), 0.0, sum); 
    }
    template<typename T>
    T lInfNorm(const std::vector<T> &v) {
        return accumulate(v.begin(), v.end(), 0, std::max<T>);
    }

    template<typename M, typename T>
    void gaussseidel (const MatrixClass<M>& a, const std::vector<T>& b, std::vector<T>& x, int iterMax)
    {
        std::vector<T> y(b.size());
        while (iterMax > 0)
        {
            for (int i = 0; i < x.size(); i++)
            {
                y[i] = (b[i] / a(i,i));
                for (int j = 0; j < x.size(); j++)
                {
                    if (j == i)
                        continue;
                    y[i] = y[i] - ((a(i,j) / a(i,i)) * x[j]);
                    x[i] = y[i];                  
                }
            printf("x%d = %f   ", i+1, y[i]);
            }
            cout << "\n\n";
            iterMax--;
        }
    }

    template <typename T>
    void MatrixClass<T>::Resize(int numRows, int numCols, const T& value)
    {
      a_.resize(numRows);
      for (int i=0;i<a_.size();++i)
      {
        a_[i].resize(numCols);
        for (int j=0;j<a_[i].size();++j)
          a_[i][j] = value;
      }
      numRows_ = numRows;
      numCols_ = numCols;
    }

    template <typename T>
    T& MatrixClass<T>::operator()(int i, int j)
    {
      if ((i<0)||(i>=numRows_))
      {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      if ((j<0)||(j>=numCols_))
      {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      return a_[i][j];
    }

    template <typename T>
    T MatrixClass<T>::operator()(int i,int j) const
    {
      if ((i<0)||(i>=numRows_))
      {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      if ((j<0)||(j>=numCols_))
      {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
      }
      return a_[i][j];
    }
    
    template <typename T>
    MatrixInterface<T>& MatrixClass<T>::operator*=(T x)
    {
      for (int i=0;i<numRows_;++i)
        for (int j=0;j<numCols_;++j)
          a_[i][j]*=x;
          
      return *this;
    }

    template <typename T>
    MatrixInterface<T>&  MatrixClass<T>::operator+=(const MatrixInterface<T>& x)
    {
      if ((x.Rows()!=numRows_)||(x.Cols()!=numCols_))
      {
        std::cerr << "Dimensions of matrix a (" << numRows_
                  << "x" << numCols_ << ") and matrix x (" 
                  << numRows_ << "x" << numCols_ << ") do not match!";
        exit(EXIT_FAILURE);
      }
      for (int i=0;i<numRows_;++i)
        for (int j=0;j<x.Cols();++j)
          a_[i][j]+=x(i,j);
      return *this;
    }

    template <typename T>
    void MatrixClass<T>::Print() const
    {
      std::cout << "(" << numRows_ << "x";
      std::cout << numCols_ << ") matrix:" << std::endl;
      for (int i=0;i<numRows_;++i)
      {
        std::cout << std::setprecision(3);
        for (int j=0;j<numCols_;++j)
            std::cout  << std::setw(5) << a_[i][j] << " ";
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }
  

	template<typename T>
	typename MatrixTraits<T>::MatrixType MatrixClass<T>::frobeniusnorm() const
	{
      typedef typename MatrixTraits<T>::MatrixType MatrixType;
      MatrixType sum = MatrixType();
    	//T sum = 0;
    	for(int i = 0; i < numRows_; ++i)
    	{
       		for(int j = 0; j < numCols_; ++j) {
       	    	sum += a_[i][j]*a_[i][j];
       	    }
    	}
      return sum;
	}
    template<class M, class P>
    typename MatrixTraits<M>::MatrixType matrixNorm ( MatrixClass<M>& A)
    {
        typedef typename MatrixTraits<M>::MatrixType MatrixType;
        MatrixType result = MatrixType();
        for(int i = 0; i < A.Rows(); i++) {
            MatrixType row_result = MatrixType();
            for (int j = 0; j < A.Cols(); j++) {
                P::EntryOp(A(i,j), row_result);
            }            
            P::RowOp(row_result, result);
        }
        return result;
    }

    
    // More arithmetic functions
   /* template <typename R, typename S>
    MatrixClass<R> operator*(const MatrixClass<R>& a, S x)
    {
      MatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename S, typename R>
    MatrixClass<R> operator*(S x, const MatrixClass<R>& a)
    {
      MatrixClass<R> temp(a);
      temp *= x;
      return temp;
    }

    template <typename R>
    MatrixClass<R> operator+(const MatrixClass<R>& a,const MatrixClass<R>& b)
    {
      MatrixClass<R> temp(a);
      temp += b;
      return temp;
    }*/

#endif