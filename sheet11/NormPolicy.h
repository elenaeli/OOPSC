#ifndef NORMPOLICY_H
#define NORMPOLICY_H
#include <iostream>
#include <cstdlib>
#include <cmath>

class FrobeniusPolicy {
    public:
        template<typename T>
        static void EntryOp(T& r, T& row_result) {
            row_result += r*r;
        }
        template<typename T>
        static void RowOp(T& row_result, T& result) {
            result += row_result;
        }
    };
    class RowSumPolicy {
    public:
        template<typename T>
        static void EntryOp(T& r, T& row_result) {
            row_result += abs(r);
        }
        template<typename T>
        static void RowOp(T& row_result, T& result) {
            result = std::max(row_result, result);
        }
    };
    class TotalPolicy {
    public:
        template<typename T>
        static void EntryOp(T& r, T& row_result) {
            row_result = std::max(row_result, abs(r));
        }
        template<typename T>
        static void RowOp(T& row_result, T& result) {
            result = std::max(row_result, result);
        }
    };

#endif