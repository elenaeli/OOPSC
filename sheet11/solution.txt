EXERCISE 1
1. What kind of variability is necessary in the algorithm?

We need to be able to say what the return type of our frobeniusnorm() function will be. Since it is not always the the type of the template parameter. For example, we can have complex<T> and then we want the frobeniusnorm() to return T and not complex<T>. That's why we want to introduce different properties of the template argument and we can achieve this variablity using traits.

2. Which traits would you want to introduce because of this?
We would like to introduce Value Traits. If we don't always use the default constructor for the matrix class, then the Type Traits are not enough and we need Value Traits. With them we can specify different types for the different constructors.

Test cases for Exercise 1. 3x3 matrices:

int
5 3 4 
2 3 2
3 6 1

float
2.51 3.42 1.84
3.42 1.37 3.91
1.84 3.91 2.51

double
3.45 2.21 3.96
1.58 3.45 2.74
2.21 3.96 1.58

Rational
1/3 3/4 2/3
1/4 2/5 4/5
2/3 3/5 3/4

complex
2.1+2*i 3.2+3*i 1.1+2*i
3.2+3*i 1.1+1*i 2.1+2*i
2.1+2*i 3.2+3*i 1.1+2*i

where (for instance in 2.1+2*i) we have a real part 2.1 and an imaginary part 2*i.



EXERCISE 2
6. The algorithm above may be inefficient, e.g. in the case of the Frobenius norm. What change
to the algorithm (and the policies) would allow more flexibility and a faster implementation?

We could use for instance a Smart Pointer to make it more flexible and faster.



Exercise 1 and 2:
Compile with
g++ -std=c++11 main.cc Rational.cpp NormPolicy.h

Output:
Matrix A1 of int = 
(3x3) matrix:
    5     3     4 
    2     3     2 
    3     6     1 

Frobenius norm of A1 = 10.630

Matrix A2 of float = 
(3x3) matrix:
2.510 3.420 1.840 
6.460 1.370 3.910 
1.840 5.910 2.510 

Frobenius norm of A2 = 11.176

Matrix A3 of double = 
(3x3) matrix:
3.450 2.210 3.960 
1.580 3.450 2.740 
2.210 3.960 1.580 

Frobenius norm of A3 = 8.800

Matrix A4 Rational = 
(3x3) matrix:
    1/3     3/4     2/3 
    1/4     2/5     4/5 
    2/3     3/5     3/4 

Frobenius norm of A4 = 1.830

Matrix A5 of complex<int> = 
(3x3) matrix:
(2,3) (5,3) (4,8) 
(6,1) (8,2) (2,3) 
(5,3) (6,1) (8,2) 

Frobenius norm of A5 = (14,8)

Matrix A6 of complex<float> = 
(3x3) matrix:
(4.600,2.000) (9.210,3.000) (0.480,7.000) 
(5.300,6.000) (6.300,4.000) (9.210,3.000) 
(2.300,5.000) (0.480,7.000) (4.600,2.000) 

Frobenius norm of A6 = (14.036,10.607)

Matrix A7 of complex<double> = 
(3x3) matrix:
(2.780,2.000) (4.660,3.000) (4.950,5.000) 
(4.950,5.000) (3.120,6.000) (2.780,2.000) 
(9.210,3.000) (9.210,6.000) (4.660,3.000) 

Frobenius norm of A7 = (16.315,11.658)

Policies: 
Matrix B = 
(3x3) matrix:
    2     1     0 
    1     3     2 
    0     1    -4 

Frobenius Norm Policy: 6.000
Row Sum Norm Policy: 6
Total Norm Policy: 12
