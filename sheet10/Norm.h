#ifndef NORM_H
#define NORM_H
#include <iostream>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <cmath>
#include "Interface.h"

template<class T>
class L2Norm : public NormInterface<T>
{
public:    
	L2Norm(std::vector<T> v_) : v(v_) {};
	~L2Norm() {};
    T calculateNorm() const {
        return std::accumulate(v.begin(), v.end(), static_cast<T>(0),
            [](T sum, T el) { return sum + el * el; });
    }
private:
    std::vector<T> v;
};

template<class T>
class L1Norm : public NormInterface<T>
{
public:    
	L1Norm(std::vector<T> v_) : v(v_) {};
	~L1Norm() {};
    T calculateNorm() const {
        auto sum = [](T temp, T val) {
            return temp + abs(val); 
        }; 
        return std::accumulate(v.begin(), v.end(), 0.0, sum); 
    }
private:
    std::vector<T> v;
};

template<class T>
class LInfNorm : public NormInterface<T>
{
public:    
	LInfNorm(std::vector<T> v_) : v(v_) {};
	~LInfNorm() {};
    T calculateNorm() const {
         return accumulate(v.begin(), v.end(), 0, std::max<T>);
    }
private:
    std::vector<T> v;
};

#endif