#ifndef MATRIXINTERFACE_H
#define MATRIXINTERFACE_H
#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;
 
template<class T>
class MatrixInterface 
{
public:
   MatrixInterface() {};
   virtual ~MatrixInterface() {};
   virtual void Resize(int numRows, int numCols, const T& value) = 0;
   virtual T& operator()(int i, int j) = 0;
   virtual T operator()(int i, int j) const = 0;   
   virtual MatrixInterface<T>& operator*=(T x) = 0;
   virtual MatrixInterface<T>& operator+=(const MatrixInterface<T>& x) = 0;
   virtual void Print() const = 0 ; 
   virtual double frobeniusnorm() const = 0;
   virtual int Rows() const = 0;
   virtual int Cols() const = 0;   

  private:
    // matrix elements
    std::vector<std::vector<T> > a_;
    // number of rows
    int numRows_;
    // number of columns
    int numCols_;
};

template<class T>
class NormInterface
{
public:
   virtual T calculateNorm() const = 0;
};

#endif
 