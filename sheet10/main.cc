#include <vector>
#include <iostream>
#include <cmath>
#include <numeric>
#include <cstdlib>
#include "MatrixClass.h"
#include "Norm.h"

using namespace std;

int main() {
    int n = 100;
    vector<double> u(n); 
    std::vector<double> a (5);
    std::cout << "Vector a = ["; 
    for(int i = 0; i < a.size(); i++) {
       	a[i] = i+1;
        std::cout << a[i] << " ";
    }
    std::cout << "]" << std::endl;

    L2Norm<double> l2(a);
    L1Norm<double> l1(a);
    LInfNorm<double> lInf(a);
	std::cout << "L2 norm: " << sqrt(l2.calculateNorm())  << std::endl;
    std::cout << "L1 norm: " << l1.calculateNorm()  << std::endl;
    std::cout << "L Infinity norm: " << lInf.calculateNorm() << std::endl << std::endl;

    MatrixClass<double> A1(4,4);
    for (int i=0;i<A1.Rows();++i)
      A1(i,i) = 3.0;
    for (int i=0;i<A1.Rows()-1;++i) 
      A1(i+1,i) = A1(i,i+1) = 2.0;
    std::cout << "Matrix A1: " << std::endl ;
    A1.Print();
    double frobNorm = A1.frobeniusnorm();
    std::cout << "Frobenius norm of matrix A1: " << frobNorm << std::endl << std::endl;

    MatrixClass<double> A2(3,3);
    A2(0,0) = 2; A2(0,1) = 1; A2(0,2) = 0;
    A2(1,0) = 1; A2(1,1) = 3; A2(1,2) = 2;
    A2(2,0) = 3; A2(2,1) = 1; A2(2,2) = -4;
    
    std::cout << "Matrix A2 = " << std::endl ;
    A2.Print();
    std::vector<double> x = {0, 1, -0.5};
    std::vector<double> b = {1, 2, 3};
    std:: cout << "x = " << "[" << x[0] << ", "<< x[1] << ", " << x[2] << "]" << std::endl;
    std:: cout << "b = " << "[" << b[0] << ", "<< b[1] << ", " << b[2] << "]" << std::endl << std::endl;
    std::cout << "Gauss Seidel of matrix A2: " << std::endl;
    gaussseidel(A2,b,x,3);

    MatrixClass<double> A3(3, 3);
    std::vector<double> x1 = {0, 0, 0};
    std::vector<double> b1 = {-6, -7, -8};
    A3(0,0) = 10; A3(0,1) = -2; A3(0,2) = -2;
    A3(1,0) = -1; A3(1,1) = 10; A3(1,2) = -1;
    A3(2,0) = -1; A3(2,1) = -1; A3(2,2) = 10;
    std::cout << "Matrix A3 = " << std::endl ;
    A3.Print();
    std::cout << "Gauss Seidel of matrix A3: " << std::endl;
    gaussseidel(A3,b1,x1,6);

}
